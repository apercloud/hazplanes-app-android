package com.hazplanes.hazplanes.data;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by imac on 06/08/14.
 */
public class HazPlanesDbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "hazplanes.db";

    public HazPlanesDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_TABLE =
                "CREATE TABLE " + HazPlanesContract.HazPlanesEntry.TABLE_NAME + " (" +
                        HazPlanesContract.HazPlanesEntry.COLUMN_PLAN_ID + " INTEGER PRIMARY KEY);";

        db.execSQL(SQL_CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + HazPlanesContract.HazPlanesEntry.TABLE_NAME);
        onCreate(db);

    }
}
