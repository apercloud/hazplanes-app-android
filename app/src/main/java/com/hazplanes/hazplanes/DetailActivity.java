package com.hazplanes.hazplanes;

import android.app.ActionBar;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.ShareActionProvider;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class DetailActivity extends ActionBarActivity {

    private AdView mAdView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        if(MainActivity.mostrarPubli) {
            mAdView = new AdView(this);
            mAdView.setAdUnitId("ca-app-pub-0179166557701407/2766495740");
            mAdView.setAdSize(com.google.android.gms.ads.AdSize.BANNER);

            FrameLayout layout = (FrameLayout) findViewById(R.id.container);

            layout.addView(mAdView);

            /// Now whenever I want to position it on the bottom I do this:
            mAdView.setLayoutParams(new FrameLayout.LayoutParams(
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    FrameLayout.LayoutParams.WRAP_CONTENT,
                    Gravity.CENTER_HORIZONTAL | Gravity.BOTTOM));

            mAdView.loadAd(new AdRequest.Builder().build());
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new DetailFragment())
                    .commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.detail, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class DetailFragment extends Fragment {

        //private static final String LOG_TAG = DetailFragment.class.getSimpleName();

        private static final String HAZPLANES_SHARE_HASHTAG = " #HazPlanes";
        private String mPlanStr;
        private String planid = "";
        private String tipoevento = "";
        private String urlTipo = "";
        private String planNombre = "";

        ShareActionProvider mShareActionProvider;

        private final ImageDownloader imageDownloader = new ImageDownloader();

        public DetailFragment() {
            setHasOptionsMenu(true);
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                Bundle savedInstanceState) {
            Intent intent = getActivity().getIntent();
            View rootView = inflater.inflate(R.layout.fragment_detail, container, false);

            float scale = getResources().getDisplayMetrics().density;
            int dpAsPixels = (int) (50 * scale + 0.5f);
            int dpAsPixels16 = 0; //(int) (16 * scale + 0.5f);
            if(MainActivity.mostrarPubli) {
                rootView.findViewById(R.id.scrollView).setPadding(0, 0, 0, dpAsPixels);
            } else {
                rootView.findViewById(R.id.scrollView).setPadding(0, 0, 0, dpAsPixels16);
            }

            if(intent != null && intent.hasExtra(Intent.EXTRA_TEXT)) {
                mPlanStr = intent.getStringExtra(Intent.EXTRA_TEXT);
                if(mPlanStr.contains("-")) {
                    String[] parts = mPlanStr.split("-");
                    planid = parts[0];
                    tipoevento = parts[1];
                }
                ((TextView) rootView.findViewById(R.id.field_titulo)).setText("");
            }

            // Hook up clicks on the thumbnail views.
            //final Button button = (Button) rootView.findViewById(R.id.zoombutton);
            final ImageView zoom = (ImageView) rootView.findViewById(R.id.zoombutton);
            final ScrollView scrollView = (ScrollView) rootView.findViewById(R.id.scrollView);
            final ImageView expandedImage = (ImageView) rootView.findViewById(R.id.expanded_image);
            zoom.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    expandedImage.setVisibility(View.VISIBLE);
                    scrollView.setVisibility(View.INVISIBLE);
                }
            });
            expandedImage.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    scrollView.setVisibility(View.VISIBLE);
                    expandedImage.setVisibility(View.INVISIBLE);
                }
            });

            return rootView;
        }

        private Intent createSharePlanIntent(String url) {
            Intent shareIntent = new Intent((Intent.ACTION_SEND));
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
            shareIntent.setType("text/plain");

            planNombre = ((TextView) getActivity().findViewById(R.id.field_titulo)).getText().toString();

            shareIntent.putExtra(Intent.EXTRA_TEXT,
                    "Mira que plan más interesante! " + planNombre + " - " + url + HAZPLANES_SHARE_HASHTAG);
            return shareIntent;
        }

        @Override
        public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
            // Inflate the menu; this adds items to the action bar if it is present.
            inflater.inflate(R.menu.detailfragment, menu);

            MenuItem menuItem = menu.findItem(R.id.action_share);

            mShareActionProvider =
                    (ShareActionProvider) MenuItemCompat.getActionProvider(menuItem);

            /*
            if(mShareActionProvider != null) {
                mShareActionProvider.setShareIntent(createSharePlanIntent());
            } else {
                Log.d(LOG_TAG, "Share Action Provider is null?");
            }*/
        }

        @Override
        public void onStart() {
            super.onStart();
            FetchPlanTask planTask = new FetchPlanTask();
            planTask.execute();
        }

        public class FetchPlanTask extends AsyncTask<Void, Void, ArrayList<EventoItem>> {

            private ProgressDialog pDialog;
            private final String LOG_TAG = FetchPlanTask.class.getSimpleName();

            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                Integer poster = R.drawable.null_calendar;
                ActionBar bar = null;
                bar = getActivity().getActionBar();

                if(tipoevento.equals("peliculas")) {
                    poster = R.drawable.null_film;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#E7007A")));
                }
                if(tipoevento.equals("conciertos")) {
                    poster = R.drawable.null_music;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#009BEE")));
                }
                if(tipoevento.equals("formacion")) {
                    poster = R.drawable.null_cursos;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#871E53")));
                }
                if(tipoevento.equals("exposiciones")) {
                    poster = R.drawable.null_museum;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#CFCF00")));
                }
                if(tipoevento.equals("deportes")) {
                    poster = R.drawable.null_sports;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#EE4116")));
                }
                if(tipoevento.equals("eventos")) {
                    poster = R.drawable.null_calendar;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#0D8731")));
                }
                if(tipoevento.equals("obrasteatro")) {
                    poster = R.drawable.null_theatre;
                    bar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#784C2A")));
                }
                (getActivity().findViewById(R.id.field_poster)).setBackgroundResource(poster);

                pDialog = new ProgressDialog(getActivity());
                pDialog.setMessage("Cogiendo datos ...");
                pDialog.setIndeterminate(false);
                pDialog.setCancelable(true);
                pDialog.show();
            }

            protected ArrayList<EventoItem> doInBackground(Void... params) {
                // These two need to be declared outside the try/catch
                // so that they can be closed in the finally block.
                HttpURLConnection urlConnection = null;
                BufferedReader reader = null;

                // Will contain the raw JSON response as a string.
                String planJsonStr;
                ArrayList<EventoItem> result;

                try {
                    URL url = new URL("http://hazplanes.com/api/" + MainActivity.urlapiversion);

                    String controller = "";
                    String idfield = "";
                    if(tipoevento.equals("peliculas")) {
                        controller = "Peliculas";
                        idfield = "idpelicula";
                        urlTipo = "pelicula";
                    }
                    if(tipoevento.equals("conciertos")) {
                        controller = "Conciertos";
                        idfield = "idconcierto";
                        urlTipo = "concierto";
                    }
                    if(tipoevento.equals("formacion")) {
                        controller = "Cursos";
                        idfield = "idcurso";
                        urlTipo = "curso";
                    }
                    if(tipoevento.equals("exposiciones")) {
                        controller = "Exposiciones";
                        idfield = "idexposicion";
                        urlTipo = "exposicion";
                    }
                    if(tipoevento.equals("deportes")) {
                        controller = "Deportes";
                        idfield = "iddeporte";
                        urlTipo = "competicion";
                    }
                    if(tipoevento.equals("eventos")) {
                        controller = "Eventos";
                        idfield = "ideventos";
                        urlTipo = "evento";
                    }
                    if(tipoevento.equals("obrasteatro")) {
                        controller = "Obras";
                        idfield = "idobrateatro";
                        urlTipo = "obra";
                    }

                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(5);
                    nameValuePairs.add(new BasicNameValuePair("controller", controller));
                    nameValuePairs.add(new BasicNameValuePair("action", "Read"));
                    nameValuePairs.add(new BasicNameValuePair(idfield, planid));

                    StringBuilder postData = new StringBuilder();
                    for (NameValuePair param : nameValuePairs) {
                        if (postData.length() != 0) postData.append('&');
                        postData.append(URLEncoder.encode(param.getName(), "UTF-8"));
                        postData.append('=');
                        postData.append(URLEncoder.encode(String.valueOf(param.getValue()), "UTF-8"));
                    }
                    byte[] postDataBytes = postData.toString().getBytes("UTF-8");

                    // Create the request to OpenWeatherMap, and open the connection
                    urlConnection = (HttpURLConnection) url.openConnection();
                    //urlConnection.setRequestMethod("GET");
                    urlConnection.setRequestMethod("POST");
                    urlConnection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
                    urlConnection.setRequestProperty("Content-Length", String.valueOf(postDataBytes.length));
                    urlConnection.setDoOutput(true);
                    urlConnection.getOutputStream().write(postDataBytes);
                    //urlConnection.connect();

                    // Read the input stream into a String
                    InputStream inputStream = urlConnection.getInputStream();
                    StringBuilder buffer = new StringBuilder();
                    if (inputStream == null) {
                        // Nothing to do.
                        return null;
                    }
                    reader = new BufferedReader(new InputStreamReader(inputStream));

                    String line;
                    while ((line = reader.readLine()) != null) {
                        // Since it's JSON, adding a newline isn't necessary (it won't affect parsing)
                        // But it does make debugging a *lot* easier if you print out the completed
                        // buffer for debugging.
                        line += "\n";
                        buffer.append(line);
                    }

                    if (buffer.length() == 0) {
                        // Stream was empty.  No point in parsing.
                        return null;
                    }
                    planJsonStr = buffer.toString();

                    JSONParser parser = new JSONParser();
                    try {
                        result = parser.getEventoDataFromJson(planJsonStr, tipoevento);
                    } catch(JSONException e) {
                        Log.e(LOG_TAG, e.getMessage(), e);
                        e.printStackTrace();
                        return null;
                    }
                } catch (IOException e) {
                    Log.e(LOG_TAG, "Error ", e);
                    return null;
                } finally {
                    if (urlConnection != null) {
                        urlConnection.disconnect();
                    }
                    if (reader != null) {
                        try {
                            reader.close();
                        } catch (final IOException e) {
                            Log.e(LOG_TAG, "Error closing stream", e);
                        }
                    }
                }
                return result;
            }

            @Override
            protected void onPostExecute(ArrayList<EventoItem> result) {

                Date dateStart = Calendar.getInstance().getTime();
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-M-d");
                float scale = getResources().getDisplayMetrics().density;
                int paddinglados = (int) (15 * scale + 0.5f);
                int paddingpolos = (int) (5 * scale + 0.5f);

                pDialog.dismiss();
                if(result != null) {
                    if(result.get(0).getReserva() != null && !result.get(0).getReserva().equals("null") && result.get(0).getReserva()) {
                        if(result.get(0).getLimitereserva() != null && !result.get(0).getLimitereserva().equals("null") && !result.get(0).getLimitereserva().equals("") && !result.get(0).getLimitereserva().equals("0000-00-00 00:00:00")) {
                            try {
                                Date fechalimite = new SimpleDateFormat("yyyy-MM-dd HH:mm").parse(result.get(0).getLimitereserva());
                                ((TextView) getActivity().findViewById(R.id.field_reserva)).setText(getResources().getString(R.string.reservabefore) + " " + new SimpleDateFormat("dd-MM-yyyy HH:mm").format(fechalimite));
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                        } else {
                            ((TextView) getActivity().findViewById(R.id.field_reserva)).setText(getResources().getString(R.string.reservaneed));
                        }
                        getActivity().findViewById(R.id.field_reserva).setVisibility(View.VISIBLE);
                    }
                    ((TextView) getActivity().findViewById(R.id.field_titulo)).setText(result.get(0).getNombre());
                    ((TextView) getActivity().findViewById(R.id.field_genero)).setText(result.get(0).getGenero());
                    getActivity().findViewById(R.id.field_genero).setVisibility(View.VISIBLE);
                    ((TextView) getActivity().findViewById(R.id.field_sinopsis)).setText(result.get(0).getSinopsis());
                    if(result.get(0).getInfantil() != null && !result.get(0).getInfantil().equals("null") && result.get(0).getInfantil()) {
                        ((TextView) getActivity().findViewById(R.id.field_infantil)).setText(getResources().getString(R.string.infantilminusculas));
                        getActivity().findViewById(R.id.field_infantil).setVisibility(View.VISIBLE);
                    }
                    if(result.get(0).getActualizado() != null && !result.get(0).getActualizado().equals("null") && !result.get(0).getActualizado().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_actualizado)).setText(getResources().getString(R.string.lastupdate) + " " + result.get(0).getActualizado());
                        getActivity().findViewById(R.id.field_actualizado).setVisibility(View.VISIBLE);
                    } else if(result.get(0).getCreado() != null && !result.get(0).getCreado().equals("null") && !result.get(0).getCreado().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_actualizado)).setText(getResources().getString(R.string.lastupdate) + " " + result.get(0).getCreado());
                        getActivity().findViewById(R.id.field_actualizado).setVisibility(View.VISIBLE);
                    }
                    if(!result.get(0).getNotas().equals("null") && !result.get(0).getNotas().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_notas)).setText(result.get(0).getNotas());
                    } else {
                        getActivity().findViewById(R.id.field_notas).setVisibility(View.GONE);
                    }
                    if(!result.get(0).getMenciones().equals("null") && !result.get(0).getMenciones().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_menciones)).setText(result.get(0).getMenciones());
                    } else {
                        getActivity().findViewById(R.id.field_menciones).setVisibility(View.GONE);
                    }
                    //Info de peliculas solo
                    Boolean showPeliInfo = false;
                    if(result.get(0).getDirector() != null && !result.get(0).getDirector().equals("null") && !result.get(0).getDirector().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_director)).setText(result.get(0).getDirector());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleDirector).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_director).setVisibility(View.GONE);
                    }
                    if(result.get(0).getActores() != null && !result.get(0).getActores().equals("null") && !result.get(0).getActores().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_actores)).setText(result.get(0).getActores());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleActores).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_actores).setVisibility(View.GONE);
                    }
                    if(result.get(0).getNacionalidad() != null && !result.get(0).getNacionalidad().equals("null") && !result.get(0).getNacionalidad().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_nacionalidad)).setText(result.get(0).getNacionalidad());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleNacionalidad).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_nacionalidad).setVisibility(View.GONE);
                    }
                    if(result.get(0).getDistribuidora() != null && !result.get(0).getDistribuidora().equals("null") && !result.get(0).getDistribuidora().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_distribuidora)).setText(result.get(0).getDistribuidora());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleDistribuidora).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_distribuidora).setVisibility(View.GONE);
                    }
                    if(result.get(0).getEdad() != null && !result.get(0).getEdad().equals("null") && !result.get(0).getEdad().equals("") && !result.get(0).getEdad().equals("0")) {
                        ((TextView) getActivity().findViewById(R.id.field_edad)).setText(result.get(0).getEdad());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleEdad).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_edad).setVisibility(View.GONE);
                    }
                    if(result.get(0).getDuracion() != null && !result.get(0).getDuracion().equals("null") && !result.get(0).getDuracion().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_duracion)).setText(result.get(0).getDuracion());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleDuracion).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_duracion).setVisibility(View.GONE);
                    }
                    if(result.get(0).getFechaestreno() != null && !result.get(0).getFechaestreno().equals("null") && !result.get(0).getFechaestreno().equals("")) {
                        ((TextView) getActivity().findViewById(R.id.field_estreno)).setText(result.get(0).getFechaestreno());
                        showPeliInfo = true;
                    } else {
                        getActivity().findViewById(R.id.titleEstreno).setVisibility(View.GONE);
                        getActivity().findViewById(R.id.field_estreno).setVisibility(View.GONE);
                    }
                    if(showPeliInfo) {
                        getActivity().findViewById(R.id.info_pelicula).setVisibility(View.VISIBLE);
                    }
                    //Fin info peliculas solo
                    if (!result.get(0).getPosterUrl().equals("http://hazplanes.com/null")) {
                        imageDownloader.download(result.get(0).getPosterUrl(), ((ImageView) getActivity().findViewById(R.id.field_poster)));
                        imageDownloader.download(result.get(0).getPosterUrl(), ((ImageView) getActivity().findViewById(R.id.expanded_image)));
                        int sdk = android.os.Build.VERSION.SDK_INT;
                        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                            (getActivity().findViewById(R.id.field_poster)).setBackgroundDrawable(null);
                        } else {
                            (getActivity().findViewById(R.id.field_poster)).setBackground(null);
                        }
                    } else {
                        ((ImageView) getActivity().findViewById(R.id.field_poster)).setImageDrawable(null);
                        ((ImageView) getActivity().findViewById(R.id.expanded_image)).setImageDrawable(null);
                    }

                    try {
                        int sdk = android.os.Build.VERSION.SDK_INT;

                        Calendar calendar = Calendar.getInstance();
                        String today = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);

                        calendar.add(Calendar.DATE, 1);

                        String tomorrow = calendar.get(Calendar.YEAR) + "-" + (calendar.get(Calendar.MONTH)+1) + "-" + calendar.get(Calendar.DATE);

                        Boolean ponerTitulo = false;
                        String local = "";
                        String nombreLocal = "";

                        // get a reference for the TableLayout
                        TableLayout table = (TableLayout) getActivity().findViewById(R.id.TableLayout01);
                        table.removeAllViews();
                        table.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

                        for(JSONArray horas : result.get(0).getSesiones()) {
                            for(int i = 0; i < horas.length(); i++) {
                                JSONObject planData = horas.getJSONObject(i);

                                //Ponemos el nombre del local o cine o auditorio si ha cambiado
                                String nombre = "nombre";
                                String direccion = "";
                                String localidad = "";
                                String coordenadas;
                                if(!planData.has(nombre)) {
                                    nombre = "lugar";
                                } else {
                                    direccion = planData.getString("direccion");
                                    localidad = planData.getString("localidad");
                                }
                                if(!planData.has("coordenadas")) {
                                    coordenadas = "";
                                } else {
                                    coordenadas = planData.getString("coordenadas");
                                }
                                final String c = coordenadas;
                                if(!planData.getString(nombre).equals(local)) {
                                    local = planData.getString(nombre);
                                    nombreLocal = local;
                                    if(!direccion.equals("")) {
                                        nombreLocal = local + "\n(" + direccion + " - " + localidad + ")";
                                    }
                                    ponerTitulo = true;
                                }
                                TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.MATCH_PARENT, TableRow.LayoutParams.WRAP_CONTENT);

                                if(planData.has("fecha")) { //Todas menos exposiciones
                                    Date d1;
                                    d1 = format.parse(planData.getString("fecha"));
                                    if (d1.getTime() >= dateStart.getTime()) {
                                        if (ponerTitulo) {
                                            // create a new TableRow
                                            TableRow rowTitle = new TableRow(getActivity());
                                            rowTitle.setLayoutParams(lp);
                                            TextView t0 = new TextView(getActivity());
                                            //Ponemos height a wrap_content para cuando la direccion es muy larga
                                            t0.setLayoutParams(new TableRow.LayoutParams(0, TableRow.LayoutParams.WRAP_CONTENT, 0.66f));
                                            t0.setText(nombreLocal);
                                            t0.setSingleLine(false);
                                            //t0.setTextColor(Color.WHITE);
                                            if (sdk > Build.VERSION_CODES.HONEYCOMB) {
                                                t0.setPaddingRelative(paddinglados, paddingpolos, paddinglados, paddingpolos);
                                            }

                                            t0.setTypeface(null, Typeface.BOLD);
                                            rowTitle.addView(t0);

                                            if (!coordenadas.equals("")) {
                                                ImageView coord = new ImageView(getActivity());
                                                coord.setOnClickListener(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        String latLong;
                                                        if (!c.substring(0, 1).equals("(")) {
                                                            latLong = c;
                                                        } else {
                                                            latLong = c.substring(1, c.length() - 1);
                                                        }
                                                        Intent mapsIntent = new Intent(android.content.Intent.ACTION_VIEW,
                                                                Uri.parse("http://maps.google.com/?q=" + latLong));
                                                        startActivity(mapsIntent);
                                                    }
                                                });
                                                /*coord.setText(R.string.viewGoogleMaps);
                                                coord.append("  ");
                                                coord.setTypeface(null, Typeface.ITALIC);
                                                coord.setTextColor(Color.parseColor("#fffe6d39"));*/
                                                coord.setImageResource(R.drawable.directions);
                                                rowTitle.addView(coord);
                                            }
                                            rowTitle.setBackgroundColor(Color.parseColor("#FFE4C4"));

                                            table.addView(rowTitle, new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.WRAP_CONTENT));

                                            ponerTitulo = false;
                                        }
                                        // create a new TableRow
                                        TableRow row = new TableRow(getActivity());
                                        row.setLayoutParams(lp);

                                        //Salas
                                        if (planData.has("numero")) {
                                            // create a new TextView
                                            TextView tSala = new TextView(getActivity());
                                            String sala = "Sala " + planData.getString("numero");
                                            if (planData.has("3d") && planData.getString("3d").equals("1")) {
                                                sala += " (3D)";
                                            }
                                            if (planData.has("vo") && planData.getString("vo").equals("1")) {
                                                sala += " (VOSE)";
                                            }
                                            // set the text to "text xx"
                                            tSala.setText(sala);
                                            tSala.setTextColor(Color.BLACK);
                                            tSala.setBackgroundColor(Color.WHITE);
                                            if (sdk > Build.VERSION_CODES.HONEYCOMB) {
                                                tSala.setPaddingRelative(paddinglados, paddingpolos, paddinglados, paddingpolos);
                                            }
                                            tSala.setGravity(16);
                                            row.addView(tSala);
                                        }
                                        // create a new TextView
                                        TextView t1 = new TextView(getActivity());
                                        t1.setLayoutParams(new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT, TableRow.LayoutParams.WRAP_CONTENT, 0.66f));
                                        String fecha = formatDate.format(d1);
                                        if (fecha.equals(today)) {
                                            fecha = new SimpleDateFormat("HH:mm").format(d1);
                                            if(fecha.equals("00:00")) {
                                                if(planData.getString("todoeldia").equals("1")) {
                                                    fecha = "todo el día";
                                                } else {
                                                    fecha = "hora sin determinar";
                                                }
                                            }
                                            fecha = "Hoy " + fecha;
                                        } else {
                                            if (fecha.equals(tomorrow)) {
                                                fecha = new SimpleDateFormat("HH:mm").format(d1);
                                                if(fecha.equals("00:00")) {
                                                    if(planData.getString("todoeldia").equals("1")) {
                                                        fecha = "todo el día";
                                                    } else {
                                                        fecha = "hora sin determinar";
                                                    }
                                                }
                                                fecha = "Mañana " + fecha;
                                            } else {
                                                fecha = new SimpleDateFormat("dd-MM-yyyy ").format(d1);
                                                String hora = new SimpleDateFormat("HH:mm").format(d1);
                                                if(hora.equals("00:00")) {
                                                    if(planData.getString("todoeldia").equals("1")) {
                                                        fecha += "Todo el día";
                                                    } else {
                                                        fecha += "Hora sin determinar";
                                                    }
                                                } else {
                                                    fecha += hora;
                                                }
                                            }
                                        }
                                        //String fecha = new SimpleDateFormat("dd-MM-yyyy HH:mm").format(d1);
                                        // set the text to "text xx"
                                        Integer fondoHora = Color.parseColor("#33aa99");
                                        if (planData.getString("cancelado").equals("1")) {
                                            //fecha = fecha + " (" + getActivity().getResources().getString(R.string.cancelado) + ")";
                                            fondoHora = Color.parseColor("#CC3300");
                                        } else if (planData.getString("agotado").equals("1")) {
                                            //fecha = fecha + " (" + getActivity().getResources().getString(R.string.agotado) + ")";
                                            fondoHora = Color.parseColor("#FF9900");
                                        }
                                        t1.setText(fecha);
                                        t1.setTextColor(Color.WHITE);
                                        t1.setBackgroundColor(fondoHora);
                                        if (sdk > Build.VERSION_CODES.HONEYCOMB) {
                                            t1.setPaddingRelative(paddinglados, paddingpolos, paddinglados, paddingpolos);
                                        }

                                        // create a new TextView
                                        TextView t2 = new TextView(getActivity());
                                        String precio = "- -";
                                        if (planData.getString("precio") != null && !planData.getString("precio").equals("")) {
                                            precio = planData.getString("precio");
                                            if (precio != null && !precio.equals("Entrada libre") && !precio.equals("Gratis") && !precio.equals("")) {
                                                precio += " €";
                                            }
                                        }
                                        // set the text to "text xx"
                                        t2.setText(precio);
                                        t2.setTextColor(Color.BLACK);
                                        t2.setBackgroundColor(Color.WHITE);
                                        if (sdk > Build.VERSION_CODES.HONEYCOMB) {
                                            t2.setPaddingRelative(paddinglados, paddingpolos, paddinglados, paddingpolos);
                                        }
                                        //t2.setWidth(200);

                                        // add the TextView and the CheckBox to the new TableRow
                                        t1.setGravity(16);
                                        t2.setGravity(16);
                                        row.addView(t1);
                                        row.addView(t2);

                                        if(planData.has("urlcompra") && !planData.getString("urlcompra").equals("")) {
                                            ImageView entrada = new ImageView(getActivity());
                                            final String urlentradas = planData.getString("urlcompra");
                                            entrada.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    Intent entradaIntent = new Intent(android.content.Intent.ACTION_VIEW,
                                                            Uri.parse(urlentradas));
                                                    startActivity(entradaIntent);
                                                }
                                            });
                                            entrada.setImageResource(R.drawable.entradas);
                                            row.addView(entrada);
                                        }
                                        row.setGravity(16);

                                        // add the TableRow to the TableLayout
                                        table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                                    }
                                } else { //Exposiciones
                                    if (ponerTitulo) {
                                        // create a new TableRow
                                        TableRow rowTitle = new TableRow(getActivity());
                                        rowTitle.setLayoutParams(lp);
                                        TextView t0 = new TextView(getActivity());
                                        t0.setText(nombreLocal);
                                        t0.setTextColor(Color.BLACK);
                                        if (sdk > Build.VERSION_CODES.HONEYCOMB) {
                                            t0.setPaddingRelative(paddinglados, paddingpolos, paddinglados, paddingpolos);
                                        }
                                        rowTitle.setBackgroundColor(Color.parseColor("#FFE4C4"));
                                        rowTitle.addView(t0);

                                        table.addView(rowTitle, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));

                                        if(!coordenadas.equals("")) {
                                            TableRow rowMaps = new TableRow(getActivity());
                                            rowMaps.setLayoutParams(lp);
                                            ImageView coord = new ImageView(getActivity());
                                            coord.setOnClickListener(new View.OnClickListener() {
                                                @Override
                                                public void onClick(View v) {
                                                    String latLong;
                                                    if(!c.substring(0,1).equals("(")) {
                                                        latLong = c;
                                                    } else {
                                                        latLong = c.substring(1,c.length() - 1);
                                                    }
                                                    Intent mapsIntent = new Intent(android.content.Intent.ACTION_VIEW,
                                                            Uri.parse("http://maps.google.com/?q=" + latLong));
                                                    startActivity(mapsIntent);
                                                }
                                            });
                                            /*coord.setText(R.string.viewGoogleMaps);
                                            coord.setTypeface(null, Typeface.ITALIC);
                                            coord.setTextColor(Color.parseColor("#fffe6d39"));*/
                                            coord.setImageResource(R.drawable.directions);
                                            rowTitle.addView(coord);

                                            table.addView(rowMaps, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                                        }
                                        ponerTitulo = false;
                                    }
                                    // create a new TableRow
                                    TableRow row = new TableRow(getActivity());
                                    row.setLayoutParams(lp);

                                    // create a new TextView
                                    TextView t1 = new TextView(getActivity());

                                    /*Integer currentDay = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
                                    String daysList[] = {"domingo", "lunes", "martes", "miercoles", "jueves", "viernes", "sabado"};
                                    String diasSemana[] = {"lunes", "martes", "miercoles", "jueves", "viernes", "sabado", "domingo"};

                                    String fecha = planData.getString(diasSemana[i]);
                                    Integer fondo = Color.GRAY;

                                    if (fecha.equals(daysList[currentDay])) {
                                        fecha = "Hoy: " + fecha;
                                        fondo = Color.LTGRAY;
                                    } else {
                                        if (fecha.equals(daysList[currentDay + 1])) {
                                            fecha = "Mañana: " + fecha;
                                            fondo = Color.GRAY;
                                        } else {
                                            if(diasSemana[i].equals("miercoles")) {
                                                diasSemana[i] = "miércoles";
                                            } else if(diasSemana[i].equals("sabado")) {
                                                diasSemana[i] = "sábado";
                                            }
                                            fecha = diasSemana[i].substring(0, 1).toUpperCase() + diasSemana[i].substring(1) + ": " + fecha;
                                            fondo = Color.GRAY;
                                        }
                                    }*/
                                    Integer fondoHora = Color.parseColor("#33aa99");
                                    String fecha = getResources().getString(R.string.museumOpening);
                                    // set the text to "text xx"
                                    t1.setText(fecha);
                                    t1.setTextColor(Color.WHITE);
                                    if (sdk > Build.VERSION_CODES.HONEYCOMB) {
                                        t1.setPaddingRelative(paddinglados, paddingpolos, paddinglados, paddingpolos);
                                    }

                                    // add the TextView and the CheckBox to the new TableRow
                                    row.addView(t1);
                                    row.setBackgroundColor(fondoHora);

                                    // add the TableRow to the TableLayout
                                    table.addView(row, new TableLayout.LayoutParams(TableLayout.LayoutParams.WRAP_CONTENT, TableLayout.LayoutParams.WRAP_CONTENT));
                                }
                            }
                        }

                        // Campo de video con un intent para que abra en youtube
                        if(!result.get(0).getVideo().equals("null") && !result.get(0).getVideo().equals("")) {
                            ImageView video = (ImageView)getActivity().findViewById(R.id.field_video);

                            Uri uri = Uri.parse(result.get(0).getVideo());
                            uri = Uri.parse("vnd.youtube:"  + uri.getQueryParameter("v"));
                            final Uri uri2 = uri;

                            video.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent youtubeIntent = new Intent(Intent.ACTION_VIEW, uri2);
                                    startActivity(youtubeIntent);
                                }
                            });
                        } else {
                            getActivity().findViewById(R.id.field_video).setVisibility(View.GONE);
                        }

                        // Campo de url de web con un intent para que abra en navegador
                        if(!result.get(0).getWeb().equals("null") && !result.get(0).getWeb().equals("")) {
                            ImageView web = (ImageView)getActivity().findViewById(R.id.field_web);
                            //web.setText(result.get(0).getWeb());
                            final String urlWeb = result.get(0).getWeb();
                            mShareActionProvider.setShareIntent(createSharePlanIntent(urlWeb));

                            web.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(urlWeb));
                                    startActivity(browserIntent);
                                }
                            });
                        } else {
                            getActivity().findViewById(R.id.field_web).setVisibility(View.GONE);
                        }

                        // Campo de url del evento en hazplanes.com con un intent para que abra en navegador.
                        // Primero se crea y se parsea la url
                        planNombre = result.get(0).getNombre();
                        String cadena = urlTipo + "/" + planid + "-" + planNombre;
                        cadena = cadena.toLowerCase();
                        cadena = cadena.replace("á", "a");
                        cadena = cadena.replace("é", "e");
                        cadena = cadena.replace("í", "i");
                        cadena = cadena.replace("ó", "o");
                        cadena = cadena.replace("ú", "u");
                        cadena = cadena.replace("ñ", "n");
                        cadena = cadena.replace("ü", "u");
                        cadena = cadena.replace(" ", "-");
                        cadena = cadena.replace(",", "");
                        cadena = cadena.replace(";", "");
                        cadena = cadena.replace(":", "");
                        cadena = cadena.replace("(", "");
                        cadena = cadena.replace(")", "");
                        cadena = cadena.replace("º", "");
                        cadena = cadena.replace("ª", "");
                        cadena = cadena.replace("&", "-");
                        cadena = cadena.replace("+", "-");
                        cadena = cadena.replace("\n", "-");
                        cadena = cadena.replace("\r\n", "-");
                        cadena = "http://hazplanes.com/" + cadena;
                        final String url = cadena;

                        mShareActionProvider.setShareIntent(createSharePlanIntent(url));

                        ImageView masInfo = (ImageView)getActivity().findViewById(R.id.masInfoLink);
                        masInfo.setOnClickListener(new View.OnClickListener() {
                           @Override
                           public void onClick(View v) {
                                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                                startActivity(browserIntent);
                            }
                        });

                    }
                    catch (JSONException e) {
                        Log.e(LOG_TAG, "Error ", e);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}

