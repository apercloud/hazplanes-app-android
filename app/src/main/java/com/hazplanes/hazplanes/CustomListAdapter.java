package com.hazplanes.hazplanes;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CustomListAdapter extends BaseAdapter {

    private ArrayList listData;

    private LayoutInflater layoutInflater;

    private final ImageDownloader imageDownloader = new ImageDownloader();

    public CustomListAdapter(Context context, ArrayList listData) {
        this.listData = listData;
        layoutInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return listData.size();
    }

    public int getItemType(String genero){
        if(genero.equals("separadorFecha")) {
            return 2;
        } else {
            return 1;
        }
    }

    @Override
    public Object getItem(int position) {
        return listData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        EventoItem eventoItem = (EventoItem) listData.get(position);

        int type = getItemType(eventoItem.getGenero());

        if(type == 2) { //Mostrar separador con fecha
            convertView = layoutInflater.inflate(R.layout.list_item_date, null);
            holder = new ViewHolder();
            holder.fecha = (TextView) convertView.findViewById(R.id.list_item_datestring);
            holder.fecha.setText(eventoItem.getNombre());
            convertView.setTag(holder);
        } else {
            convertView = layoutInflater.inflate(R.layout.list_item_event, null);
            holder = new ViewHolder();
            holder.planid = (TextView) convertView.findViewById(R.id.list_item_id);
            holder.nombre = (TextView) convertView.findViewById(R.id.list_item_titulo);
            holder.genero = (TextView) convertView.findViewById(R.id.list_item_genero);
            holder.fecha = (TextView) convertView.findViewById(R.id.list_item_fecha);
            holder.reserva = (TextView) convertView.findViewById(R.id.list_item_reserva);
            holder.tipoevento = (TextView) convertView.findViewById(R.id.list_item_tipoevento);
            holder.dist = (TextView) convertView.findViewById(R.id.list_item_dist);
            holder.poster = (ImageView) convertView.findViewById(R.id.imageView);
            convertView.setTag(holder);

            String tipoevento = eventoItem.getTipoevento();

            holder.planid.setText(eventoItem.getID() + "-" + tipoevento);
            holder.nombre.setEllipsize(TextUtils.TruncateAt.END);
            holder.nombre.setMaxLines(2);
            holder.nombre.setText(eventoItem.getNombre());
            holder.genero.setText(eventoItem.getGenero());

            holder.tipoevento.setTextColor(Color.WHITE);
            if (tipoevento.equals("peliculas")) {
                holder.poster.setBackgroundResource(R.drawable.null_film);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#E7007A"));
            }
            if (tipoevento.equals("conciertos")) {
                holder.poster.setBackgroundResource(R.drawable.null_music);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#009BEE"));
            }
            if (tipoevento.equals("eventos")) {
                holder.poster.setBackgroundResource(R.drawable.null_calendar);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#0D8731"));
            }
            if (tipoevento.equals("exposiciones")) {
                holder.poster.setBackgroundResource(R.drawable.null_museum);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#FFF600"));
                holder.tipoevento.setTextColor(Color.BLACK);
            }
            if (tipoevento.equals("formacion")) {
                holder.poster.setBackgroundResource(R.drawable.null_cursos);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#871E53"));
            }
            if (tipoevento.equals("deportes")) {
                holder.poster.setBackgroundResource(R.drawable.null_sports);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#EE4116"));
            }
            if (tipoevento.equals("obrasteatro")) {
                holder.poster.setBackgroundResource(R.drawable.null_theatre);
                holder.tipoevento.setBackgroundColor(Color.parseColor("#784C2A"));
            }
            holder.tipoevento.setText(eventoItem.getTipoevento().substring(0, 1).toUpperCase() + eventoItem.getTipoevento().substring(1));

            // Custom date format
            String fecha = "";
            if (!tipoevento.equals("exposiciones")) {
                Date dateStart = Calendar.getInstance().getTime();
                String dateStop = eventoItem.getFecha();

                //HH converts hour in 24 hours format (0-23), day calculation
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                Date d1;
                Date d2;
                String dias = "";
                String horas;

                try {
                    d1 = dateStart;
                    d2 = format.parse(dateStop);

                    //in milliseconds
                    long diff = d2.getTime() - d1.getTime();

                    //long diffSeconds = diff / 1000 % 60;
                    long diffMinutes = diff / (60 * 1000) % 60;
                    long diffHours = diff / (60 * 60 * 1000) % 24;
                    long diffDays = diff / (24 * 60 * 60 * 1000);

                    if (diffHours > 0) {
                        horas = diffHours + "h ";
                    } else {
                        horas = "";
                    }

                    if (diffDays > 0 || diffHours > 0 || diffMinutes > 0) {
                        fecha = "En: ";
                    } else {
                        fecha = "Lleva: ";
                    }
                    String f = new SimpleDateFormat("HH:mm").format(d2);
                    if(f.equals("00:00")) {
                        if(eventoItem.getTodoeldia()) {
                            fecha = "Todo el día";
                        } else {
                            fecha = "Hora sin determinar";
                        }
                    } else {
                        if (diffDays < 1) {
                            fecha = "Hora: " + new SimpleDateFormat("HH:mm").format(d2) + " (" + fecha + dias + horas + String.format("%02d", diffMinutes) + "m)";
                        } else {
                            fecha = "Hora: " + new SimpleDateFormat("HH:mm").format(d2);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                fecha = layoutInflater.getContext().getResources().getString(R.string.museumOpening);
            }
            if(eventoItem.getCancelado()) {
                fecha = layoutInflater.getContext().getResources().getString(R.string.cancelado);
            } else if(eventoItem.getAgotado()) {
                fecha = layoutInflater.getContext().getResources().getString(R.string.agotado);
            }
            holder.fecha.setText(fecha);
            if(eventoItem.getInfantil()) {
                if(eventoItem.getReserva()) {
                    holder.reserva.setText("INFANTIL - RESERVAR");
                    holder.reserva.setTextColor(Color.parseColor("#006699"));
                } else {
                    holder.reserva.setText("INFANTIL");
                    holder.reserva.setTextColor(Color.parseColor("#8DE2FF"));
                }
            } else if(eventoItem.getReserva()) {
                holder.reserva.setText("RESERVAR");
                holder.reserva.setTextColor(Color.parseColor("#006699"));
            }

            double[] coord = eventoItem.getCoordenadas();
            if(coord[0] != 0 && coord[1] != 0 && MainActivity.clat != 0 && MainActivity.clong != 0) {
                double latitude = coord[0];
                double longitude = coord[1];
                float[] result = new float[1];

                Location.distanceBetween(MainActivity.clat, MainActivity.clong, latitude, longitude, result);

                float metros = result[0];
                String distancia = String.format("%.0f", metros) + "m";
                if (metros > 999) {
                    metros = metros / 1000;
                    distancia = String.format("%.2f", metros) + " km";
                }
                //Log.e("Dist", "Dist: " + distancia);
                holder.dist.setText("Dist: " + distancia);
            }

            if (!eventoItem.getPosterUrl().equals("http://hazplanes.com/null")) {
                imageDownloader.download(eventoItem.getPosterUrl(), holder.poster);
                int sdk = android.os.Build.VERSION.SDK_INT;
                if (sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
                    holder.poster.setBackgroundDrawable(null);
                } else {
                    holder.poster.setBackground(null);
                }
            } else {
                holder.poster.setImageDrawable(null);
            }
        }

        return convertView;
    }

    static class ViewHolder {
        TextView nombre;
        TextView fecha;
        TextView genero;
        TextView reserva;
        ImageView poster;
        TextView planid;
        TextView tipoevento;
        TextView dist;
    }
}
